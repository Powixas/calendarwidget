import { TestBed } from '@angular/core/testing';

import { WebApiMockupService } from './web-api-mockup.service';

describe('WebApiMockupServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebApiMockupService = TestBed.get(WebApiMockupService);
    expect(service).toBeTruthy();
  });
});
