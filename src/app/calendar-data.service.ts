import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventData } from './event-data';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CalendarDataService {
  constructor(private http: HttpClient) {}

  private url = 'api/events';

  getEvents(): Observable<EventData[]> {
    return this.http.get<EventData[]>(this.url)
      .pipe(catchError(this.handleError<EventData[]>('getEvents', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }
}
