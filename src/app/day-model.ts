import { EventData } from './event-data'

export class DayModel {
  date: Date;
  events: EventData[];
}
