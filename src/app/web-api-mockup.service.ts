import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

import { EventData } from './event-data'

@Injectable({
  providedIn: 'root'
})
export class WebApiMockupService implements InMemoryDbService {
  createDb() {
    const now = new Date();
    const date1 = new Date();
    date1.setDate(now.getDate() - 5);

    const date2 = new Date();
    date2.setDate(now.getDate() - 3);

    const events: Array<EventData> = [
      {
        date: date1,
        price: 15.99,
        quantity: 3,
        eventType: "Duck tape",
        isExpenseType: false,
        isHoursEventType: true,
        isAdditionalHoursEventType: false,
        isWorkHour: false,
        isApproved: false,
        isRejected: false,
        tasksCount: 1,
        firstTaskStart: null,
        lastTaskEnd: null
      },
      {
        date:date1,
        price: null,
        quantity: null,
        eventType: "Trainings",
        isExpenseType: false,
        isHoursEventType: true,
        isAdditionalHoursEventType: false,
        isWorkHour: false,
        isApproved: true,
        isRejected: false,
        tasksCount: 1,
        firstTaskStart: null,
        lastTaskEnd: null
      },
      {
        date: date1,
        price: null,
        quantity: null,
        eventType: "Approved",
        isExpenseType: false,
        isHoursEventType: true,
        isAdditionalHoursEventType: true,
        isWorkHour: false,
        isApproved: true,
        isRejected: false,
        tasksCount: 1,
        firstTaskStart: null,
        lastTaskEnd: null
      },
      {
        date: date2,
        price: null,
        quantity: null,
        eventType: "Rejected",
        isExpenseType: false,
        isHoursEventType: true,
        isAdditionalHoursEventType: true,
        isWorkHour: false,
        isApproved: false,
        isRejected: true,
        tasksCount: 1,
        firstTaskStart: null,
        lastTaskEnd: null
      },
      {
        date: now,
        price: null,
        quantity: null,
        eventType: "EV Type Other",
        isExpenseType: false,
        isHoursEventType: true,
        isAdditionalHoursEventType: true,
        isWorkHour: false,
        isApproved: false,
        isRejected: false,
        tasksCount: 1,
        firstTaskStart: null,
        lastTaskEnd: null
      }
      ];

    return { events }
  }
}
