import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatDividerModule,
  MatTooltipModule,
  MatGridListModule,
  MatBadgeModule
} from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatTooltipModule,
    MatGridListModule,
    MatBadgeModule
  ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatTooltipModule,
    MatGridListModule,
    MatBadgeModule
  ]
})
export class AngularMaterialModule { }
