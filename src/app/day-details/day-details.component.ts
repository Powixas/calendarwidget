import { Component, OnInit, Input } from '@angular/core';
import { DayModel } from '../day-model'
import { EventData } from '../event-data';

@Component({
  selector: 'app-day-details',
  templateUrl: './day-details.component.html',
  styleUrls: ['./day-details.component.css']
})
export class DayDetailsComponent implements OnInit {
  constructor() { }

  @Input() selectedDay: DayModel;

  ngOnInit() {
  }

  getHourEventTypes(): Array<EventData> {
    let result = new Array<EventData>()
    if (this.selectedDay) {
      result = this.selectedDay.events.filter(e => e.isHoursEventType);
      console.log(result);
    }

    return result;
  }

}
