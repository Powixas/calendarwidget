import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaysAreaComponent } from './days-area.component';

describe('DaysAreaComponent', () => {
  let component: DaysAreaComponent;
  let fixture: ComponentFixture<DaysAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaysAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaysAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
