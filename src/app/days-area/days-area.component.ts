import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { DayModel } from '../day-model';

@Component({
  selector: 'app-days-area',
  templateUrl: './days-area.component.html',
  styleUrls: ['./days-area.component.css']
})
export class DaysAreaComponent implements OnInit {
  constructor() { }

  @Input() selectedDay: DayModel; 
  @Input() today: DayModel;
  @Input()
  set displayDays(displayDays: Array<DayModel>) {
    this._displayDays = displayDays
    this.today = this.displayDays[6];
    this.goToToday();
  }
  get displayDays() {
    return this._displayDays;
  }

  @Output() dayChanged = new EventEmitter<DayModel>();

  private _displayDays: Array<DayModel>;

  ngOnInit() {
  }

  getShortWeekdayName(day: DayModel): string {
    const result = day.date.toLocaleString('en-us', { weekday: 'short' })

    return result;
  }

  getDayStatus(day: DayModel): string {
    if (!day.events.length) {
      return 'empty';
    }

    const isRejected = day.events.some(e => e.isRejected);
    if (isRejected) {
      return 'rejected';
    }

    const isApproved = day.events.some(e => e.isApproved);
    if (isApproved) {
      return 'approved';
    }

    return '';
  }

  getFirstEventStartTime(day: DayModel): string {
    const result = day.events.length
      ? day.events[0].firstTaskStart.toLocaleString('en-us', { hour: 'numeric', minute: 'numeric', hour12: false })
      : '-';

    return result;
  }

  isWeekendDay(day: DayModel) {
    const sundayIndex = 0;
    const saturdayIndex = 6;

    const result = day.date.getDay() == sundayIndex || day.date.getDay() == saturdayIndex;

    return result;
  }

  selectedDayChanged(day: DayModel) {
    this.selectedDay = day;
    this.dayChanged.emit(day);
  }

  goToToday() {
    this.selectedDayChanged(this.today);
  }

  getTitleDate(): string {
    const result = this.selectedDay
      ? this.selectedDay.date.toLocaleString('en-us', { month: 'long', year: 'numeric' })
      : '';

    return result;
  }


}
