import { Component, OnInit, Input } from '@angular/core';
import { CalendarDataService } from '../calendar-data.service';
import { DayModel } from '../day-model';
import { EventData } from '../event-data';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})

export class CalendarComponent implements OnInit {
  constructor(private calendarDataService: CalendarDataService) { }
  displayDays = new Array<DayModel>();
  selectedDay: DayModel;

  ngOnInit() {
    this.loadEvents();
  }

  onSelectedDayChanged(day: DayModel) {
    this.selectedDay = day;
  }

  private loadEvents() {
    this.calendarDataService.getEvents().subscribe(events => {
      events.forEach(e => {
        e.date = new Date(e.date);
        e.firstTaskStart = new Date(e.firstTaskStart);
        e.lastTaskEnd = new Date(e.lastTaskEnd);
      });
      events.sort((a, b) => {
        return a.date.getTime() - b.date.getTime();
      });
      this.initializeDisplayDays(events)
    })

  }

  private initializeDisplayDays(events: EventData[]) {
    const dayOfMonth = new Date().getDate();

    this.displayDays.length = 0;
    for (var i = 6; i >= 0; i--) {
      let dayModel = new DayModel();

      dayModel.date = new Date();
      dayModel.date.setDate(dayOfMonth - i);
      dayModel.events = events.filter(e => e.date.getDay() === dayModel.date.getDay());

      this.displayDays.push(dayModel);
    }
  }
}
